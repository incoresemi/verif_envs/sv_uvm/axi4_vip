
  
class axi_master_write_seq extends axi_sequence;

 
  
    `uvm_object_utils(axi_master_write_seq)
  
  
  function new(string name = "axi_master_write_seq");
    super.new(name);
  endfunction
  
  bit [31:0] waddr;
  bit [3:0] length;
  bit [3:0] awid;
  bit [3:0] wid;
  bit [128:0] wdata;
  
  virtual task body();
    
    req = axi_seq_item::type_id::create("req");
    wait_for_grant();
    req.randomize();
   
    req.write_start_i     = 1'b1;
       
    req.write_awid_i      = this.awid;
    req.write_addr_i      = this.waddr;
    req.write_length_i    = this.length;
    req.write_data_i      = this.wdata;
    req.write_size_i      = 3'b100;
    req.write_burst_i     = 2'b00;
    req.write_lock_i      = 2'b00;
    
    req.write_wid_i       = this.wid;
    req.write_datav_i     = 1'b1;
    req.write_strb_i      = 4'b1111;
    req.read_start_i      = 1'b0;
    
    send_request(req);
    wait_for_item_done();
    
  endtask : body
  
endclass : axi_master_write_seq