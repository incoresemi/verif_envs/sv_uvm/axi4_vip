
class axi_master_wr_burst_test extends axi_master_base_test;

  `uvm_component_utils( axi_master_wr_burst_test )
   
  axi_master_wr_burst_seq wbseq;

  function new(string name = " axi_master_wr_burst_test ",uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    wbseq =  axi_master_wr_burst_seq ::type_id::create("wbseq");
  endfunction : build_phase
  
  task run_phase(uvm_phase phase);
    
    phase.raise_objection(this);
    
    wbseq.waddr   = 32'h0;
    wbseq.length  = 4'b0101;
    wbseq.awid    = 4'b0001;
    wbseq.wid     = 4'b0001;
    wbseq.wdata    = $urandom_range(50,70);
    
    
    wbseq.start(env.axi_agnt.sequencer);
    
        
    phase.drop_objection(this);
    
   
    phase.phase_done.set_drain_time(this, 500);
    
  endtask : run_phase
  
endclass :  axi_master_wr_burst_test 