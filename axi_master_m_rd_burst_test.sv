
  class axi_master_m_rd_burst_test extends axi_master_base_test;

  `uvm_component_utils(axi_master_m_rd_burst_test)
  
  
    
  axi_master_rd_burst_seq rbseq;
  int data_r = 5;   

 
   
  function new(string name = "axi_master_m_rd_burst_test",uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  
  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);

    
    rbseq = axi_master_rd_burst_seq::type_id::create("rbseq");
  endfunction : build_phase
  
  
  task run_phase(uvm_phase phase);
    
      
    phase.raise_objection(this);
    rbseq.raddr     = 32'h0;
    rbseq.length    = 4'b0101;
    rbseq.arid      = 4'b0001;
    
    for(int i=0;i<data_r;i++) begin
      rbseq.raddr = rbseq.raddr + 4;
      rbseq.arid  = 4'b0001 + i; 
      rbseq.start(env.axi_agnt.sequencer);
    end    
   
    
    phase.drop_objection(this);
    
    
    phase.phase_done.set_drain_time(this, 500);
  endtask : run_phase
  

endclass : axi_master_m_rd_burst_test